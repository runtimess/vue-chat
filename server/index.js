const ws = require('ws')
const log = require('debug')('chat')

class Chat {
  constructor(opts) {

    this.ws = new ws.Server(opts)
    this.watch()
 
  }

  watch() {
    log('CHAT LIVE')

    this.ws.on('connection', (socket, req) => {
      log('COOKIE', req.headers.cookie)
      socket.onmessage = this.onMesssage.bind(this, socket)
      socket.onclose = this.onClose.bind(this, socket)
      socket.onerror = this.onError.bind(this, socket)

    })
  }

  onMesssage(socket, msg) {

    const {type, payload} = this.parseData(msg.data)
    
    if (type === 'MESSAGE') {
      
      const {msg} = payload

      this.ws.clients.forEach(client => {
        
        client.send(JSON.stringify({
          type: 'MESSAGE',
          payload: {
            username: socket.username,
            msg, time: this.getTime(),
            uniq: socket.uniq
          }
        }))

      })

    }

    if (type === 'JOIN') {

      const {username, uniq} = payload
      
      socket.uniq = uniq
      socket.username = username
      
      log('JOIN', username, uniq)

      this.sendData(socket, {
        type: 'JOIN_SUCCESS',
        payload: {username, uniq}
      })
      
      this.ws.clients.forEach(client => {

        const clients = []
        
        this.ws.clients.forEach(client => {

          clients.push({
            uniq: client.uniq,
            username: client.username
          })
        
        })

        client.send(JSON.stringify({
          type: 'ONLINE',
          payload: clients
        }))
      
      })
    }
  }

  onClose(socket, payload) {


    log('LEAVE', socket.username, socket.uniq)

    this.ws.clients.forEach(client => {
      
      const clients = []
      
      this.ws.clients.forEach(client => {

        clients.push({
          username: client.username,
          uniq: client.uniq
        })

      })

      client.send(JSON.stringify({
        type: 'UPDATE_ONLINE',
        payload: clients
      }))

    })

  }

  onError(socket, payload) {

    console.log('ERROR:', socket.uniq)
  
  }

  parseData(data) {
    
    return JSON.parse(data)
  
  }

  sendData(socket, payload) {
    socket.send(JSON.stringify(payload))
  }

  getTime() {
    
    const date = new Date
    const h = date.getHours()
    const m = date.getMinutes()

    return (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m)
  
  }

  getUniq() {
  
    return Math.floor(Math.random() * 9999999)
  
  }
}

const chat = new Chat({port: 7777})