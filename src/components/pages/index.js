import Join from '~/components/pages/Join'
import Chat from '~/components/pages/Chat'
import NotFound from '~/components/pages/NotFound'

export default {
  Join, Chat, NotFound
}