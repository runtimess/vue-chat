import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'

// import SocketController from '~/plugins/SocketController'
import App from '~/components/App'
import router from '~/routes/'
import store from '~/store/'

// Vue.use(SocketController)

new Vue({
	el: '#root',
	router,
	store,
	render: create => create(App)
})