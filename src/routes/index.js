import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'


import Pages from '~/components/pages/'
import store from '~/store/'



Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {path: '/', component: Pages.Chat, meta: {guard: true}},
    {path: '/join', component: Pages.Join},
    {path: '*', component: Pages.NotFound}
  ]
})


router.beforeEach((to, from, next) => {
  if (to.meta.guard) {
    return store.state.user ? next() : next({path: '/join'})
  }
  next()
})

export default router